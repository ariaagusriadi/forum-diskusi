<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::group(['middleware' => ['auth']], function () {

    // dashboard
    Route::get('/dashboard' ,'DashboardController@index');
    Route::get('/dashboard/{dashboard}', 'DashboardController@show');
    Route::get('/dashboard/{dashboard}/edit', 'DashboardController@edit');
    // Route::patch('/dashboard/{dashboard}', 'DashboardController@update');
    Route::resource('/dashboard', 'DashboardController')->only([
        'update'
    ]);
    Route::post('/dashboard', 'DashboardController@store');
    Route::delete('/dashboard/{dashboard}', 'DashboardController@destroy');

    // kategori
    Route::resource('kategori', 'KategoriController');

    // profile
    Route::resource('profile', 'ProfileController')->only([
        'index', 'update'
    ]);

    Route::resource('pertanyaan', 'PertanyaanController');

});
