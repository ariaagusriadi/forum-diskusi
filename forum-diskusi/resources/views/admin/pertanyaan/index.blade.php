@extends('admin.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Data Pertanyaan
                    <a href="/pertanyaan/create" class="btn btn-primary float-right"><i class="fa fa-plus"></i> Tambah
                        Pertanyaan</a>
                </div>
                <div class="card-body">
                    <div class="row">
                        @forelse ($pertanyaan as $item)
                            <div class="col-lg-12">
                                <div class="card mb-4">
                                    <div
                                        class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                        <h6 class="m-0 font-weight-bold text-primary">{{ $item->judul }}</h6>
                                    </div>
                                    <div class="card-body">
                                        <hr>
                                        <img src="{{ asset('images/' . $item->foto) }}" class="img-fluid"
                                            alt="Responsive image">
                                        <br>
                                        <hr>
                                        <h6 class="m-0 font-weight-bold text-dark"> Pertanyaan</h6>
                                        <span class="badge badge-pill badge-primary">{{ $item->kategori->nama }}</span>
                                        <hr>

                                        <p> {!! nl2br($item->isi) !!}</p>
                                    </div>
                                    <div class="card-footer">
                                        <hr>
                                        <div class="btn-group float-right">
                                            <div class="btn">
                                                <a href="/pertanyaan/{{ $item->id }}/edit" class="btn btn-warning"><i
                                                        class="fa fa-edit"></i> Edit</a>
                                            </div>
                                            <div class="btn">
                                                <form action="/pertanyaan/{{ $item->id }}" method="post">
                                                    @csrf
                                                    @method('delete')
                                                    <button onclick="confirm('yakin hapus pertanyaan?')"
                                                        class="btn btn-danger"><i class="fa fa-trash"></i>
                                                        Delete</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <h1>Belum Ada Pertanyaan</h1>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
