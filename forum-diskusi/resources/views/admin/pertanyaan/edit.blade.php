@extends('admin.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Tambah pertanyaan
                </div>
                <div class="card-body">
                    <form action="/pertanyaan/{{ $pertanyaan->id }}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label>Judul Pertanyaan</label>
                            <input type="text" class="form-control @error('judul') is-invalid @enderror" placeholder="judul"
                                name="judul" autofocus value="{{ $pertanyaan->judul }}">
                        </div>
                        @error('judul')
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                {{ $message }}
                            </div>
                        @enderror

                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Kategori Pertanyaan</label>
                            <select class="form-control" id="exampleFormControlSelect1" name="kategori_id">
                                @foreach ($kategori as $category)
                                    @if (old('kategori_id', $pertanyaan->kategori_id) == $category->id)
                                        <option value="{{ $category->id }}" selected>{{ $category->nama }}</option>x
                                    @else
                                        <option value="{{ $category->id }}">{{ $category->nama }}</option>x
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        @error('kategori_id')
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                {{ $message }}
                            </div>
                        @enderror

                        <div class="form-group">
                            <label>Foto Lampiran</label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input  @error('foto') is-invalid @enderror"
                                    id="customFile" name="foto" accept="image/*">
                                <label class="custom-file-label" for="customFile">File</label>
                            </div>
                        </div>
                        @error('foto')
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                {{ $message }}
                            </div>
                        @enderror


                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Pertanyaan</label>
                            <textarea class="form-control @error('isi') is-invalid @enderror" id="textarea" rows="3" name="isi">
                                {!! nl2br($pertanyaan->isi) !!}
                            </textarea>
                        </div>
                        @error('isi')
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                {{ $message }}
                            </div>
                        @enderror

                        <button class="btn btn-primary float-right"><i class="fa fa-save"></i> Submit</button>
                    </form>
                </div>
                <div class="card-footer">
                    <a href="/pertanyaan" class="btn btn-light btn-icon-split">
                        <span class="icon text-gray-600">
                            <i class="fas fa-arrow-left"></i>
                        </span>
                        <span class="text">Back</span>
                    </a>
                </div>
            </div>
        </div>

    </div>
@endsection



@push('script')
    <script src="https://cdn.tiny.cloud/1/jdn7ttnz7i2yd7893s3m1khawfp32k6gwtx2dlyjank0k3z8/tinymce/6/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
            toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
            toolbar_mode: 'floating',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
        });
    </script>
@endpush
