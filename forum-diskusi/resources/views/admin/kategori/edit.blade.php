@extends('admin.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Tambah Kategori
                </div>
                <div class="card-body">
                    <form action="/kategori/{{ $kategori->id }}" method="post">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label>Nama Kategori</label>
                            <input type="text" class="form-control @error('nama') is-invalid @enderror"
                                placeholder="Enter Nama Kategori" name="nama" value="{{ $kategori->nama }}">
                        </div>
                        @error('nama')
                            <div class="alert alert-danger alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                {{ $message }}
                            </div>
                        @enderror
                        <button class="btn btn-primary float-right"><i class="fa fa-save"></i> Submit</button>
                    </form>
                </div>
                <div class="card-footer">
                    <a href="/kategori" class="btn btn-light btn-icon-split">
                        <span class="icon text-gray-600">
                            <i class="fas fa-arrow-left"></i>
                        </span>
                        <span class="text">Back</span>
                    </a>
                </div>
            </div>
        </div>

    </div>
@endsection
