@extends('admin.app')

@section('content')

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                   Detail Data Kategori

                </div>
                <div class="card-body">
                    Kategori   : {{ $kategori->nama }}
                </div>
                <div class="card-footer">
                    <a href="/kategori" class="btn btn-light btn-icon-split">
                        <span class="icon text-gray-600">
                          <i class="fas fa-arrow-left"></i>
                        </span>
                        <span class="text">Back</span>
                      </a>
                </div>
            </div>
        </div>

    </div>
@endsection
