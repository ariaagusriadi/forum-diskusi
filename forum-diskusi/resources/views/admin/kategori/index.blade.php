@extends('admin.app')

@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ session('success') }}
        </div>
    @elseif (session()->has('edit'))
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ session('edit') }}
        </div>
    @elseif (session()->has('delete'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ session('delete') }}
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Data Kategori
                    <a href="/kategori/create" class="btn btn-primary float-right"><i class="fa fa-plus"></i> Tambah
                        Kategori</a>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($kategori as $key => $item)
                                    <tr>
                                        <th scope="row">{{ $key + 1 }}</th>
                                        <td>{{ $item->nama }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <div class="btn">
                                                    <a href="/kategori/{{ $item->id }}" class="btn btn-dark"><i
                                                            class="fa fa-info"></i> Info</a>
                                                </div>
                                                <div class="btn">
                                                    <a href="/kategori/{{ $item->id }}/edit" class="btn btn-warning"><i
                                                            class="fa fa-edit"></i> Edit</a>
                                                </div>
                                                <div class="btn">
                                                    <form action="/kategori/{{ $item->id }}" method="post">
                                                        @csrf
                                                        @method('delete')
                                                        <button onclick="confirm('yakin hapus kategori?')" class="btn btn-danger"><i class="fa fa-trash"></i>
                                                            Delete</button>
                                                    </form>
                                                </div>
                                            </div>

                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td>0</td>
                                        <td>Masih Kosong</td>
                                        <td>Masin Kosong</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
