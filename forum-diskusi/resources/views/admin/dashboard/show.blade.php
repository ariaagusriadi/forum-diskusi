@extends('admin.app')

@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ session('success') }}
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Halaman Forum Diskusi

                    <a href="/dashboard" class="btn btn-light btn-icon-split float-right">
                        <span class="icon text-gray-600">
                            <i class="fas fa-arrow-left"></i>
                        </span>
                        <span class="text">Back</span>
                    </a>
                </div>
                <div class="card-body">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h4 class="m-0 font-weight-bold text-primary">{{ $pertanyaan->judul }}</h4>
                    </div>
                    <hr>
                    <img src="{{ asset('images/' . $pertanyaan->foto) }}" class="img-fluid" alt="Responsive image">
                    <br>
                    <hr>
                    <h4 class="m-0 font-weight-bold text-dark"> Pertanyaan </h4>
                    <h6 class="m-0 font-weight-bold text-primary">by: {{ $pertanyaan->user->name }} </h6>
                    <span class="badge badge-pill badge-primary">{{ $pertanyaan->kategori->nama }}</span>
                    <hr>
                    <p>{!! nl2br($pertanyaan->isi) !!}</p>
                    <br>
                    <hr>

                    <div class="card">
                        <div class="card-header">
                            <h4 class="m-0 font-weight-bold text-dark"> Jawaban </h4>
                        </div>
                        <div class="card-body">
                            <form action="/dashboard" method="post">
                                @csrf
                                <input type="hidden" name="pertanyaan_id" value="{{ $pertanyaan->id }}">
                                <div class="form-group">
                                    <textarea class="form-control @error('isi') is-invalid @enderror" id="textarea" rows="3" name="isi">
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-dark float-right"><i class="fa fa-save"></i> Jawab</button>
                                </div>

                            </form>
                        </div>
                    </div>
                    <hr>
                    <h4 class="m-0 font-weight-bold text-dark"> List Jawaban </h4>
                    <hr>
                    @forelse ($pertanyaan->comment as $item)
                        <br>
                        <div class="card">
                            <div class="card-header">
                                {{ $item->user->name }}
                            </div>
                            <div class="card-body">
                                <blockquote class="blockquote mb-0">
                                    <p>{!! nl2br($item->isi) !!}
                                    </p>
                                </blockquote>
                            </div>
                            @if ($item->user_id == auth::user()->id)
                                <div class="card-footer">
                                    <div class="btn-group float-right">
                                        <div class="btn">
                                            <!-- Button trigger modal -->
                                            <a href="#" data-id="{{ $item->id }}" class="btn btn-warning btn-edit"><i class="fa fa-edit"></i> Edit Jawaban</a>

                                        </div>
                                        <div class="btn">
                                            <form action="/dashboard/{{ $item->id }}" method="post">
                                                @csrf
                                                @method('delete')
                                                <button onclick="confirm('yakin hapus pertanyaan?')"
                                                    class="btn btn-danger"><i class="fa fa-trash"></i>
                                                    Delete</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    @empty
                        <h1>Belum ada komentar</h1>
                    @endforelse
                </div>
            </div>
        </div>

    </div>



    <!-- Modal -->
    <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog"
        aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/test" method="post" id="form-edit">
                    @csrf
                    @method('put')
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary btn-update">Save changes</button>
                    </div>
                </form>

            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="https://cdn.tiny.cloud/1/jdn7ttnz7i2yd7893s3m1khawfp32k6gwtx2dlyjank0k3z8/tinymce/6/tinymce.min.js"
        referrerpolicy="origin"></script>
    <script>
        tinymce.init({
            selector: 'textarea',
            plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
            toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
            toolbar_mode: 'floating',
            tinycomments_mode: 'embedded',
            tinycomments_author: 'Author name',
        });

        $('.btn-edit').on('click' , function(){
            // console.log($(this).data('id'))
            let id = $(this).data('id')
            $.ajax({
                url: `/dashboard/${id}/edit`,
                method:"GET",
                success: function(data){
                    // console.log(data)
                    $('#modal-edit').find('.modal-body').html(data)
                    $('#modal-edit').modal('show')
                },
                error:function(error){
                    console.log(error)
                }
            })
        })
        $('.btn-update').on('click' , function(){
            // console.log($(this).data('id'))
            let id = $('#form-edit').find('#id_data').val()
            let formData = $('#form-edit').serialize()
            console.log(id)
            console.log(formData)
            $.ajax({
                url: `/dashboard/${id}`,
                method:"PATCH",
                data:formData,
                success: function(data){
                    // console.log(data)
                    // $('#modal-edit').find('.modal-body').html(data)
                    $('#modal-edit').modal('hide')
                    window.location.assign('/dashboard')
                },
                error:function(error){
                    console.log(error)
                }
            })
        })
    </script>

@endpush
