<input type="hidden" name="pertanyaan_id" value="{{ $comment->pertanyaan->id }}">
<input name="id" type="hidden" value="{{ $comment->id }}" id="id_data">
<div class="form-group">
    <textarea class="form-control @error('isi') is-invalid @enderror" id="textarea" rows="3" name="isi">
        {!! nl2br($comment->isi) !!}

     </textarea>
</div>
