@extends('admin.app')

@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{ session('success') }}
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Halaman Forum Diskusi
                </div>
                <div class="card-body">
                    @forelse ($pertanyaan as $item)
                        <div class="col-lg-12">
                            <div class="card mb-4">
                                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                    <h4 class="m-0 font-weight-bold text-primary">{{ $item->judul }}</h4>
                                </div>
                                <div class="card-body">
                                    <hr>
                                    <img src="{{ asset('images/' . $item->foto) }}" class="img-fluid"
                                        alt="Responsive image">
                                    <hr>
                                    <h6 class="m-0 font-weight-bold text-primary">by: {{ $item->user->name }} </h6>
                                    <span class="badge badge-pill badge-primary">{{ $item->kategori->nama }}</span>
                                    <hr>
                                    <p>{!! nl2br($item->isi) !!}</p>
                                </div>
                                <div class="card-footer">
                                    <hr>
                                    <div class="btn-group float-right">
                                        <div class="btn">
                                            <a href="/dashboard/{{ $item->id }}" class="btn btn-dark"><i
                                                    class="fa-regular fa-comments"></i> Jawaban</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @empty
                        <h1>Belum Ada Pertanyaan</h1>
                    @endforelse
                </div>
            </div>
        </div>

    </div>
@endsection
