@extends('admin.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    Data Profile
                </div>
                <div class="card-body">
                    <form action="/profile/{{ $profile->id }}" method="post">
                        @csrf
                        @method('put')
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Nama</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" value="{{ $profile->user->name }}" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">Email</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" value="{{ $profile->user->email }}" disabled>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="umur" class="col-md-4 col-form-label text-md-right">Umur</label>

                            <div class="col-md-6">
                                <input id="umur" type="number" class="form-control @error('umur') is-invalid @enderror"
                                    name="umur" value="{{ $profile->umur }}">

                                @error('umur')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="alamat" class="col-md-4 col-form-label text-md-right">Alamat</label>

                            <div class="col-md-6">

                                <textarea name="alamat" id="alamat" class="form-control  @error('alamat') is-invalid @enderror">
                                    {{ $profile->alamat }}
                                </textarea>

                                @error('alamat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="bio" class="col-md-4 col-form-label text-md-right">bio</label>

                            <div class="col-md-6">
                                <textarea name="bio" id="bio" class="form-control  @error('bio') is-invalid @enderror">
                                    {{ $profile->bio }}
                                </textarea>

                                @error('bio')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <button class="btn btn-primary float-right"><i class="fa fa-save"></i> Submit</button>
                    </form>
                </div>
                <div class="card-footer">
                    <a href="/dashboard" class="btn btn-light btn-icon-split">
                        <span class="icon text-gray-600">
                            <i class="fas fa-arrow-left"></i>
                        </span>
                        <span class="text">Back</span>
                    </a>
                </div>
            </div>
        </div>

    </div>
@endsection
