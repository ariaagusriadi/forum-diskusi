<?php

namespace App\Http\Controllers;

use App\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{

    public function index()
    {
        $kategori = Kategori::all();
        return view('admin.kategori.index', compact('kategori'));
    }

    public function create()
    {
        return view('admin.kategori.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        $kategori = new Kategori();
        $kategori->nama = request('nama');
        $kategori->save();

        return redirect('kategori')->with('success','berhasil manambahkan kategori baru');
    }


    public function show($id)
    {
        $kategori = Kategori::find($id);
        return view('admin.kategori.show', compact('kategori'));
    }

    public function edit($id)
    {
        $kategori = Kategori::find($id);
        return view('admin.kategori.edit', compact('kategori'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required'
        ]);
        $kategori = Kategori::find($id);
        $kategori->nama = request('nama');
        $kategori->save();

        return redirect('kategori')->with('edit','berhasil Edit kategori');
    }


    public function destroy($id)
    {
        $kategori = Kategori::find($id);
        $kategori->delete();
        return redirect('kategori')->with('delete','berhasil Delete kategori');

    }
}
