<?php

namespace App\Http\Controllers;

use App\Kategori;
use App\pertanyaan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use RealRashid\SweetAlert\Facades\Alert;


class PertanyaanController extends Controller
{

    public function index()
    {
        $pertanyaan = pertanyaan::where('user_id', auth()->user()->id)->get();
        return view('admin.pertanyaan.index', compact('pertanyaan'));
    }


    public function create()
    {
        $kategori = Kategori::all();
        return view('admin.pertanyaan.create', compact('kategori'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'kategori_id' => 'required',
            'foto' => 'required',
            'isi' => 'required',
        ]);

        $fotoname = time() . '.' . $request->foto->extension();
        $request->foto->move(public_path('images'), $fotoname);

        $pertanyaan = new pertanyaan();
        $pertanyaan->user_id = auth()->user()->id;
        $pertanyaan->judul = request('judul');
        $pertanyaan->kategori_id = request('kategori_id');
        $pertanyaan->foto = $fotoname;
        $pertanyaan->isi = request('isi');
        $pertanyaan->save();
        Alert::success('Success Add ', 'Success Add Pertanyaan');

        return redirect('/pertanyaan');
    }


    public function show(pertanyaan $pertanyaan)
    {
        //
    }


    public function edit($id)
    {
        $kategori = Kategori::all();
        $pertanyaan = pertanyaan::find($id);
        return view('admin.pertanyaan.edit', compact('pertanyaan', 'kategori'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'kategori_id' => 'required',
            'isi' => 'required',
        ]);

        $pertanyaan = pertanyaan::find($id);
        if ($request->has('foto')) {
            $path = 'images/';
            File::delete($path . $pertanyaan->foto);
            $fotoname = time() . '.' . $request->foto->extension();
            $request->foto->move(public_path('images'), $fotoname);
            $pertanyaan->foto = $fotoname;
        }

        $pertanyaan->user_id = auth()->user()->id;
        $pertanyaan->judul = request('judul');
        $pertanyaan->kategori_id = request('kategori_id');
        $pertanyaan->isi = request('isi');
        $pertanyaan->save();
        Alert::success('Success Edit ', 'Success Edit Pertanyaan');

        return redirect('/pertanyaan');
    }


    public function destroy($id)
    {
        $pertanyaan = pertanyaan::find($id);
        $pertanyaan->delete();

        $path = 'images/';
        File::delete($path. $pertanyaan->foto);
        alert()->warning('Delete Pertanyaan','kamu telah mengahapus pertanyaan');

        return redirect('/pertanyaan');
    }
}
