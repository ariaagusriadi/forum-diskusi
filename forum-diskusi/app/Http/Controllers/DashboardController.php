<?php

namespace App\Http\Controllers;

use App\Comment;
use App\pertanyaan;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;


class DashboardController extends Controller
{
    public function index()
    {
        $pertanyaan = pertanyaan::all();
        return view('admin.dashboard.index', compact('pertanyaan'));
    }

    public function show($id)
    {
        $pertanyaan = pertanyaan::find($id);
        return view('admin.dashboard.show', compact('pertanyaan'));
    }

    public function store(Request $request)
    {

        $komentar = new Comment();
        $komentar->user_id = auth()->user()->id;
        $komentar->pertanyaan_id = $request->pertanyaan_id;
        $komentar->isi = $request->isi;
        $komentar->save();
        Alert::success('Success Comment', 'Success Comment');
        return redirect('/dashboard/' . $request->pertanyaan_id);
    }

    public function destroy($id)
    {
        $coment = Comment::find($id);
        $coment->delete();
        Alert::warning('Warning Delete', 'Warning delete');

        return back();
    }

    public function edit($id)
    {
        $comment = Comment::find($id);
        return view('admin.dashboard.edit', compact('comment'));
        // echo('aria');

    }

    public function update(Request $request, $id)
    {
        // Comment::where('id', $id)->update([
        //     'pertanyaan_id' => $request->pertanyaan_id,
        //     'isi' => $request->isi,
        //     'user_id' => auth()->user()->id
        // ]);
        $komentar = Comment::find($id);
        $komentar->user_id = auth()->user()->id;
        $komentar->pertanyaan_id = $request->pertanyaan_id;
        $komentar->isi = $request->isi;
        $komentar->save();

    }
}
