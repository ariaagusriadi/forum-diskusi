<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //

    protected $fillable = [
        'user_id',
        'pertanyaan_id',
        'isi'
    ];


    public function user()
    {
        return $this->belongsTo('App\user', 'user_id');
    }


    public function pertanyaan()
    {
        return $this->belongsTo('App\pertanyaan', 'pertanyaan_id');
    }
}
