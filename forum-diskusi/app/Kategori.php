<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    //

    protected $filable = ['id' , 'nama'];


    public function pertanyaan()
    {
        return $this->hasOne('App\pertanyaan');
    }
}
