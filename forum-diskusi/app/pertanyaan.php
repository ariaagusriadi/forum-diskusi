<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pertanyaan extends Model
{

    protected $table = 'pertanyaans';
    protected $fillable = [
        'judul',
        'isi',
        'foto',
        'kategori_id',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\user', 'user_id');
    }

    public function kategori()
    {
        return $this->belongsTo('App\kategori', 'kategori_id');
    }

    public function comment()
    {
        return $this->hasMany('App\comment');
    }
}
